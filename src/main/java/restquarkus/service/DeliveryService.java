package restquarkus.service;

import restquarkus.data.Delivery;

import javax.enterprise.context.RequestScoped;
import java.util.List;

@RequestScoped
public class DeliveryService {
    public List<Delivery> getDeliveries() {
        return Delivery.listAll();
    }
}
