package restquarkus.dto;

import lombok.Data;

@Data
public class DeliveryDto {
    public String name;
}
