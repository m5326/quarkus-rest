package restquarkus.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import restquarkus.data.Delivery;
import restquarkus.dto.DeliveryDto;

import java.util.List;

@Mapper(componentModel = "cdi")
public interface DeliveryMapper {
    List<DeliveryDto> toResource(List<Delivery> delivery);
}
