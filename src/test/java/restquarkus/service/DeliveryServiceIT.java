package restquarkus.service;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import restquarkus.api.DeliveryResource;
import restquarkus.data.Delivery;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static io.restassured.RestAssured.when;

@QuarkusTest
@TestHTTPEndpoint(DeliveryResource.class)
public class DeliveryServiceIT {

    @BeforeAll
    @Transactional
    public static void setup() {
        Delivery d = new Delivery();
        d.name = "test2";
        d.persist();
    }

    @Test
    public void testGetDeliveries() {
        String name = when().get().then().extract().jsonPath().getString("name[0]");
        System.out.println(when().get().jsonPath().prettify());
        Assertions.assertEquals("test2", name);
    }
}
