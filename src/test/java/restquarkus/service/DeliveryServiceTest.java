package restquarkus.service;

import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.common.DevServicesContext;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import restquarkus.data.Delivery;

import javax.inject.Inject;
import java.util.ArrayList;

@QuarkusTest
class DeliveryServiceTest {

    @Inject
    DeliveryService ds;

    @BeforeAll
    public static void setup() {
        PanacheMock.mock(Delivery.class);
        Delivery d = new Delivery();
        d.name = "test";
        ArrayList al = new ArrayList();
        al.add(d);
        PanacheMock.doReturn(al).when(Delivery.class).listAll();
    }
    @Test
    public void getDBDeliveries() {
        System.out.println(ds.getDeliveries());
    }
}